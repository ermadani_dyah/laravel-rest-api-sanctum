<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForgentKeyNameIdToPengirimenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengirimen', function (Blueprint $table) {
            $table->char('pengiriman_status_id', 36);
            $table->foreign('pengiriman_status_id')->references('id')->on('pengirimian_statuses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengirimen', function (Blueprint $table) {
            $table->dropForeign('pengiriman_status_id');
        });
    }
}
