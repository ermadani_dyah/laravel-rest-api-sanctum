<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
</head>
<body>
    <style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Laporan Kasir</h4>
		<h6>
            <a target="_blank" href="https://www.malasngoding.com/membuat-laporan-…n-dompdf-laravel/">www.malasngoding.com</a>
        </h5>
	</center>
 
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Kasir</th>
				<th>Foto</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($kasir as $kasirs)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{$kasirs->nama_kasir}}</td>
				<td>
                    <img src="/dump/kasir/{{$kasirs->foto}}" width="100">
                </td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>