<?php

namespace App\Http\Requests;

use App\Models\TipeBarang;
use Illuminate\Foundation\Http\FormRequest;

class StoreBarang extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_barang_id' => 'required|numeric|exists:' . TipeBarang::class . ',id',
            'nama_barang' => 'required|string',
            'harga' => 'required|numeric',
            'gambar' => 'required'
        ];
    }
}
