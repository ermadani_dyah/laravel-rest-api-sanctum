<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Kurir;
use App\Models\PengirimianStatus;
use App\Models\Transaksi;
class StorePengiriman extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kurir_id' => 'required|numeric|exists:' . Kurir::class . ',id',
            'pengiriman_status_id' => 'required|numeric|exists:' . PengirimianStatus::class . ',id',
            'transaksi_id' => 'required|numeric|exists:' . Transaksi::class . ',id',
            'tanggal_pengiriman' => 'required|date'
        ];
    }
}
