<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImportKurir extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'data' => 'required|array',
            'data.*.nama_kurir' => 'required|string',
            'data.*.logo' => 'required'
        ];
    }
}
