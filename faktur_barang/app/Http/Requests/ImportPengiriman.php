<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Transaksi;
class ImportPengiriman extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'data' => 'required|array',
            'data.*.kurir' => 'required|string' ,
            'data.*.pengiriman_status' => 'required|string',
            'data.*.transaksi_id' => 'required|numeric|exists:' . Transaksi::class . ',id',
            'data.*.tanggal_pengiriman' => 'required|date'
        ];
    }
}
