<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImportTransaksi extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'data' => 'required|array',
            'data.*.kasir' => 'required|string',
            'data.*.pelanggan' => 'required|string',
            'data.*.tanggal_transaksi' => 'required|date'
        ];
    }
}
