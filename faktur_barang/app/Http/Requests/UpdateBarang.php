<?php

namespace App\Http\Requests;

use App\Models\TipeBarang;
use Illuminate\Foundation\Http\FormRequest;

class UpdateBarang extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_barang_id' => 'sometimes|required|numeric|exists:' . TipeBarang::class . ',id',
            'nama_barang' => 'sometimes|required|string',
            'harga' => 'sometimes|required|numeric',
            'gambar' => 'sometimes|required'
        ];
    }
}
