<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImportKasir extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'data' => 'required|array',
            'data.*.nama_kasir' => 'required|string',
            'data.*.foto' => 'required'
        ];
    }
}
