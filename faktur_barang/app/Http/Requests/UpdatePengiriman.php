<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Kurir;
use App\Models\PengirimianStatus;
use App\Models\Transaksi;
class UpdatePengiriman extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kurir_id' => 'sometimes|required|numeric|exists:' . Kurir::class . ',id',
            'pengiriman_status_id' => 'sometimes|required|string|exists:' . PengirimianStatus::class . ',id',
            'transaksi_id' => 'sometimes|required|numeric|exists:' . Transaksi::class . ',id',
            'tanggal_pengiriman' => 'sometimes|required|date'
        ];
    }
}
