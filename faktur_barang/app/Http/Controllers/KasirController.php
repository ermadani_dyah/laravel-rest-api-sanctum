<?php

namespace App\Http\Controllers;
use App\Models\Kasir;
use App\Http\Requests\StoreKasir;
use App\Http\Requests\UpdateKasir;
use App\Http\Requests\ImportKasir;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\UploadDump;
use PDF;
use App\Exports\KasirExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
class KasirController extends Controller
{
    CONST FETCHED_ATTRIBUTE = [
        "nama_kasir",
        "foto"
    ];
    public function index()
    {
        $kasir = Kasir::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Kasir',
            'data'    => $kasir 
        ], 200);
    }
    public function show(Kasir $kasir)
    {
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Kasir',
            'data'    => $kasir
        ], 200);
    }
    public function store(StoreKasir $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        if($data['foto']){
            UploadDump::where('rel_path', $data['foto'])->update(array('link_status'=>1));
        }
        $kasir = Kasir::create($data);
        if($kasir) {
            //jika data berhasil ditambah
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Ditambah',
                'data'    => $kasir
            ], 201);

        }else{
            //jika data gagal ditambah
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Ditambah',
            ], 409);
            }
    }
    public function update(UpdateKasir $request, Kasir $kasir)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        if($data['foto'] != $kasir->foto){
            UploadDump::where('rel_path', $kasir->foto)->update(array('link_status'=>0));
            UploadDump::where('rel_path', $data['foto'])->update(array('link_status'=>1));
        }
        $kasir->update($data);
        if($kasir) {
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Diupdate',
                'data'    => $kasir 
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Diupdate!',
            ], 500);
        }
    }
    public function destroy(Kasir $kasir)
    {
        if($kasir->foto){
            UploadDump::where('rel_path', $kasir->foto)->update(array('link_status'=>0));
        }
        $kasir->delete();
        if($kasir){
            //data berhasil dihapus
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus',
            ], 200);
        }else{
            //data gagal dihapus
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Dihapus!',
            ], 500);
        }
    }
    public function destroyAll()
    {
        $kasir = Kasir::truncate();
        return response()->json([
            'success' => true,
            'message' => 'Data Berhasil Dihapus',
        ], 200);
    }
    public function exportPdf()
    {
    	$kasir = Kasir::all();
    	$pdf = PDF::loadview('export-pdf-kasir',['kasir'=>$kasir]);
    	return $pdf->download('export-kasir.pdf');
        // return view('export-pdf-kasir',['kasir'=>$kasir]);
    }
    public function exportExcel()
    {
        // $kasir = new KasirExport;
        // Excel::loadView('export-excel-kasir', array('kasir'=>$kasir))->export('xls');
    	// // $excel = Excel::loadview('export-excel-kasir',['kasir'=>$kasir]);
    	// // return $excel->download('export-kasir.xlsx');
        // return view('export-excel-kasir',['kasir'=>$kasir]);
        return Excel::download(new KasirExport, 'kasir.xlsx');
    }
    public function import(ImportKasir $request)
    {
        $data = $request->data;
        $result = [];
        foreach ($data as $d) {
            if($d['foto']){
                $uploadDump = UploadDump::where('rel_path', $d['foto'])->first();
                if ($uploadDump) {
                    $uploadDump->update(array('link_status'=>1));
                }
            }
            $result[] = Kasir::create($d);
        }
        return response($result);
    }
}
