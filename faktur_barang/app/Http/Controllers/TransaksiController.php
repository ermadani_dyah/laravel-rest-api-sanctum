<?php

namespace App\Http\Controllers;
use App\Models\Barang;
use App\Models\Transaksi;
use App\Models\Kasir;
use App\Models\Pelanggan;
use App\Models\TransaksiDetail;
use App\Http\Requests\StoreTransaksi;
use App\Http\Requests\UpdateTransaksi;
use App\Http\Requests\ImportTransaksi;
use App\Exports\TransaksiExport;
use App\Exports\TransaksiAllExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use PDF;
use DB;
use Maatwebsite\Excel\Facades\Excel;
class TransaksiController extends Controller
{
    CONST FETCHED_ATTRIBUTE = [
        "kasir_id",
        "pelanggan_id",
        "barang_id",
        "kuantitas",
        "tanggal_transaksi",
        "total",
        "detail_transaksi"
    ];
    public function index()
    {
        $detail = Transaksi::with(['details.barangs','kasirs','pelanggans'])->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Transaksi',
            'data'    => $detail
        ], 200); 

    }
    public function show(Transaksi $transaksi)
    {
        $transaksi->load(['details.barangs','kasirs','pelanggans']);
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Transaksi',
            'data'    => $transaksi
        ], 200);
    }
    public function store(StoreTransaksi $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $data['total']=0;
        $transaksi = Transaksi::create($data);
        $total=0;
        foreach($data['detail_transaksi'] as $item){
            $barang=Barang::find($item['barang_id']);
            $hargaBarang=$barang->harga;
            $detail=array(
                'transaksi_id'=>$transaksi->id,
                'barang_id'=>$item['barang_id'],
                'kuantitas'=>$item['kuantitas']
            );
            $total+=$hargaBarang*$item['kuantitas'];
            $details=TransaksiDetail::create($detail);
        }
        $transaksi->update([
            'total' => $total
        ]);
        $detail_transaksi=array(
            $transaksi,
            $detail
        );
        if($transaksi) {
            //jika data berhasil ditambah
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Ditambah',
                'data'    => $detail_transaksi
            ], 201);

        }else{
            //jika data gagal ditambah
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Ditambah',
            ], 409);
        }
    }
    public function update(UpdateTransaksi $request, Transaksi $transaksi)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $data['total']=0;
        $transaksi->update($data);
        TransaksiDetail::where('transaksi_id',$transaksi->id)->delete();
        $total=0;
        foreach($data['detail_transaksi'] as $item){
            $barang=Barang::find($item['barang_id']);
            $hargaBarang=$barang->harga;
            $detail=array(
                'transaksi_id'=>$transaksi->id,
                'barang_id'=>$item['barang_id'],
                'kuantitas'=>$item['kuantitas']
            );
            $total+=$hargaBarang*$item['kuantitas'];
            $details=TransaksiDetail::create($detail);
        }
        $transaksi->update([
            'total' => $total
        ]);
        $detail_transaksi=array(
            $transaksi,
            $detail
        );
        if($transaksi) {
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Diupdate',
                'data'    => $detail_transaksi 
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Diupdate!',
            ], 500);
        }
    }
    public function destroy($id)
    {
        //find by ID
        $transaksi=TransaksiDetail::where('transaksi_id',$id);
        $transaksi->delete();
        Transaksi::where('id',$id)->delete();
        if($transaksi){
            //data berhasil dihapus
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus',
            ], 200);
        }else{
            //data gagal dihapus
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Dihapus!',
            ], 500);
        } 
    }
    public function destroyAll()
    {
        $transaksi = Transaksi::truncate();
        return response()->json([
            'success' => true,
            'message' => 'Data Berhasil Dihapus',
        ], 200);
    }
    public function exportPdf(Transaksi $transaksi)
    {
    	$transaksi->load(['details.barangs','kasirs','pelanggans']);
    	$pdf = PDF::loadview('export-pdf-transaksi',['transaksi'=>$transaksi]);
        $timestamp = Carbon::now()->unix();
    	return $pdf->download('export-transaksi-'.$timestamp.'.pdf');
        // return view('export-pdf-transaksi',['transaksi'=>$transaksi]);
    }
    public function exportExcel()
    {
        $timestamp = Carbon::now()->unix();
        return Excel::download(new TransaksiAllExport, 'transaksi'.$timestamp.'.xlsx');
    }
    public function exportToExcel(Request $request)
    {
        $timestamp = Carbon::now()->unix();
        return Excel::download(new TransaksiExport(
            $request->from,
            $request->to
        ), 'Transaksi-' . $request->from . '-' . $request->to .'-date-'.$timestamp. '.xlsx');
    }
    public function import(ImportTransaksi $request)
    {
        $data = $request->data;
        $result = [];
        foreach ($data as $d) {
            $kasir = Kasir::where('nama_kasir', $d['kasir'])->first();
            $pelanggan = Pelanggan::where('nama_pelanggan', $d['pelanggan'])->first();
            $d['kasir_id'] = $kasir->id;
            $d['pelanggan_id'] = $pelanggan->id;
            $d['total']=0;
            $transaksi = Transaksi::create($d);
            $total=0;
                foreach($d['detail_transaksi'] as $item){
                    $barang = Barang::where('nama_barang', $item['barang'])->first();
                    $item['barang_id'] = $barang->id;
                    $hargaBarang=$barang->harga;
                    $detail=array(
                        'transaksi_id'=>$transaksi->id,
                        'barang_id'=>$item['barang_id'],
                        'kuantitas'=>$item['kuantitas']
                    );
                    $total+=$hargaBarang*$item['kuantitas'];
                    $details=TransaksiDetail::create($detail);
                } 
                $transaksi->update(['total'=>$total]);
                $result[]=$transaksi;
                // dd($transaksi);           
        }
        return response($result);
    }
}
