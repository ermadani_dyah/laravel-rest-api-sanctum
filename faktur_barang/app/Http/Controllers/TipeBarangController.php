<?php

namespace App\Http\Controllers;

use App\Models\TipeBarang;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTipeBarang;
use App\Http\Requests\UpdateTipeBarang;
use App\Http\Requests\ImportTipeBarang;
use Illuminate\Support\Facades\Validator;
class TipeBarangController extends Controller
{
    CONST FETCHED_ATTRIBUTE = [
        "tipe_barang"
    ];
    public function index()
    {
        $TipeBarang = TipeBarang::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Tipe Barang',
            'data'    => $TipeBarang
        ], 200);

    }
    public function show(TipeBarang $TipeBarang)
    {
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Tipe Barang',
            'data'    => $TipeBarang
        ], 200);
    }
    public function store(StoreTipeBarang $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $TipeBarang = TipeBarang::create($data);
        if($TipeBarang) {
            //jika data berhasil ditambah
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Ditambah',
                'data'    => $TipeBarang
            ], 201);

        }else{
            //jika data gagal ditambah
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Ditambah',
            ], 409);
        }
    }
    public function update(UpdateTipeBarang $request, TipeBarang $TipeBarang)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $TipeBarang->update($data);
        if($TipeBarang) {
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Diupdate',
                'data'    => $TipeBarang 
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Diupdate!',
            ], 500);
        }
    }
    public function destroy(TipeBarang $TipeBarang)
    {
        $TipeBarang->delete();
        if($TipeBarang){
            //data berhasil dihapus
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus',
            ], 200);
        }else{
            //data gagal dihapus
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Dihapus!',
            ], 500);
        }
    }
    public function destroyAll()
    {
        $TipeBarang = TipeBarang::truncate();
        return response()->json([
            'success' => true,
            'message' => 'Data Berhasil Dihapus',
        ], 200);
    }
    public function import(ImportTipeBarang $request)
    {
        $data = $request->data;
        $result = [];
        foreach ($data as $d) {
            $result[] = TipeBarang::create($d);
        }
        return response($result);
    }
}
