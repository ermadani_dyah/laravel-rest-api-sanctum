<?php

namespace App\Http\Controllers;

use App\Models\PengirimianStatus;
use Illuminate\Http\Request;
use App\Http\Requests\StorePengirimanStatus;
use App\Http\Requests\UpdatePengirimanStatus;
use App\Http\Requests\ImportPengirimanStatus;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
class PengirimianStatusController extends Controller
{
    CONST FETCHED_ATTRIBUTE = [
        "nama_status",
        "id"
    ];
    public function index()
    {
        $PengirimianStatus = PengirimianStatus::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Pengiriman Status',
            'data'    => $PengirimianStatus
        ], 200);
    }
    public function show(PengirimianStatus $pengirimanStatus)
    {
        // dd($PengirimianStatus);
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Pengiriman Status',
            'data'    => $pengirimanStatus
        ], 200);
    }
    public function store(StorePengirimanStatus $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $id = Str::random(1);
        $data['id']=$id;
        $PengirimianStatus = PengirimianStatus::create($data);
        if($PengirimianStatus) {
            //jika data berhasil ditambah
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Ditambah',
                'data'    => $PengirimianStatus
            ], 201);

        }else{
            //jika data gagal ditambah
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Ditambah',
            ], 409);
        }
    }
    public function update(UpdatePengirimanStatus $request, PengirimianStatus $pengirimanStatus)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $pengirimanStatus->update($data);
        if($pengirimanStatus) {
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Diupdate',
                'data'    => $pengirimanStatus
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Diupdate!',
            ], 500);
        }
    }
    public function destroy(PengirimianStatus $pengirimanStatus)
    {
        $pengirimanStatus->delete();
        if($pengirimanStatus){
            //data berhasil dihapus
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus',
            ], 200);
        }else{
            //data gagal dihapus
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Dihapus!',
            ], 500);
        }
    }
    public function destroyAll()
    {
        $pengirimanStatus = PengirimianStatus::truncate();
        return response()->json([
            'success' => true,
            'message' => 'Data Berhasil Dihapus',
        ], 200);
    }
    public function import(ImportPengirimanStatus $request)
    {
        $data = $request->data;
        $result = [];
        foreach ($data as $d) {
            $id = Str::random(1);
            $d['id']=$id;
            $result[] = PengirimianStatus::create($d);
        }
        return response($result);
    }
}
