<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use App\Http\Requests\StorePelanggan;
use App\Http\Requests\UpdatePelanggan;
use App\Http\Requests\ImportPelanggan;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\UploadDump;
use Illuminate\Support\Facades\Validator;
class PelangganController extends Controller
{
    CONST FETCHED_ATTRIBUTE = [
        "nama_pelanggan",
        "photo"
    ];
    public function index()
    {
        $pelanggan = Pelanggan::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Pelanggan',
            'data'    => $pelanggan 
        ], 200);
    }
    public function show(Pelanggan $pelanggan)
    {
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Pelanggan',
            'data'    => $pelanggan
        ], 200);
    }
    public function store(StorePelanggan $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        if($data['photo']){
            UploadDump::where('rel_path', $data['photo'])->update(array('link_status'=>1));
        }
        $pelanggan = Pelanggan::create($data);
        if($pelanggan) {
            //jika data berhasil ditambah
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Ditambah',
                'data'    => $pelanggan
            ], 201);

        }else{
            //jika data gagal ditambah
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Ditambah',
            ], 409);
        }
    }
    public function update(UpdatePelanggan $request, Pelanggan $pelanggan)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        if($data['photo'] != $pelanggan->photo){
            UploadDump::where('rel_path', $pelanggan->photo)->update(array('link_status'=>0));
            UploadDump::where('rel_path', $data['photo'])->update(array('link_status'=>1));
        }
        $pelanggan->update($data);
        if($pelanggan) {
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Diupdate',
                'data'    => $pelanggan
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Diupdate!',
            ], 500);
        }
    }
    public function destroy(Pelanggan $pelanggan)
    {
        if($pelanggan->photo){
            UploadDump::where('rel_path', $pelanggan->photo)->update(array('link_status'=>0));
        }
        $pelanggan->delete();
        if($pelanggan){
            //data berhasil dihapus
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus',
            ], 200);
        }else{
            //data gagal dihapus
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Dihapus!',
            ], 500);
        }
    }
    public function destroyAll()
    {
        $pelanggan = Pelanggan::truncate();
        return response()->json([
            'success' => true,
            'message' => 'Data Berhasil Dihapus',
        ], 200);
    }
    public function import(ImportPelanggan $request)
    {
        $data = $request->data;
        $result = [];
        foreach ($data as $d) {
            if($d['photo']){
                $uploadDump = UploadDump::where('rel_path', $d['photo'])->first();
                if ($uploadDump) {
                    $uploadDump->update(array('link_status'=>1));
                }
            }
            $result[] = Pelanggan::create($d);
        }
        return response($result);
    }
}
