<?php

namespace App\Http\Controllers;

use App\Models\Kurir;
use App\Models\UploadDump;
use App\Http\Requests\StoreKurir;
use App\Http\Requests\UpdateKurir;
use App\Http\Requests\ImportKurir;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
class KurirController extends Controller
{
    CONST FETCHED_ATTRIBUTE = [
        "nama_kurir",
        "logo"
    ];
    public function index()
    {
        $kurir = Kurir::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Kurir',
            'data'    => $kurir 
        ], 200);
    }
    public function show(Kurir $kurir)
    {
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Kurir',
            'data'    => $kurir
        ], 200);
    }
    public function store(StoreKurir $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        if($data['logo']){
            UploadDump::where('rel_path', $data['logo'])->update(array('link_status'=>1));
        }
        $kurir = Kurir::create($data);
        if($kurir) {
            //jika data berhasil ditambah
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Ditambah',
                'data'    => $kurir
            ], 201);

        }else{
            //jika data gagal ditambah
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Ditambah',
            ], 409);
        }
    }
    public function update(UpdateKurir $request, Kurir $kurir)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        if($data['logo'] != $kurir->logo){
            UploadDump::where('rel_path', $kurir->logo)->update(array('link_status'=>0));
            UploadDump::where('rel_path', $data['logo'])->update(array('link_status'=>1));
        }
        $kurir->update($data);

        if($kurir) {
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Diupdate',
                'data'    => $kurir
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Diupdate!',
            ], 500);
        }
    }
    public function destroy(Kurir $kurir)
    {
        if($kurir->logo){
            UploadDump::where('rel_path', $kurir->logo)->update(array('link_status'=>0));
        }
        $kurir->delete();
        if($kurir){
            //data berhasil dihapus
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus',
            ], 200);
        }else{
            //data gagal dihapus
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Dihapus!',
            ], 500);
        }
    }
    public function destroyAll()
    {
        $kurir = Kurir::truncate();
        return response()->json([
            'success' => true,
            'message' => 'Data Berhasil Dihapus',
        ], 200);
    }
    public function import(ImportKurir $request)
    {
        $data = $request->data;
        $result = [];
        foreach ($data as $d) {
            if($d['logo']){
                $uploadDump = UploadDump::where('rel_path', $d['logo'])->first();
                if ($uploadDump) {
                    $uploadDump->update(array('link_status'=>1));
                }
            }
            $result[] = Kurir::create($d);
        }
        return response($result);
    }
}
