<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreLogin;
class LoginController extends Controller
{   
    CONST FETCHED_ATTRIBUTE = [
        "email",
        "password"
    ];
    public function index(StoreLogin $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $user= User::where('email', $data['email'])->first();
        
            if (!$user || !Hash::check($data['password'], $user->password)) {
                return response([
                    'success'   => false,
                    'message' => ['These credentials do not match our records.']
                ], 404);
            }
        
            $token = $user->createToken('ApiToken')->plainTextToken;
        
            $response = [
                'success'   => true,
                'user'      => $user,
                'token'     => $token
            ];
        
        return response($response, 201);
    }
    public function logout()
    {
        auth()->logout();
        return response()->json([
            'success'    => true
        ], 200);
    }

}
