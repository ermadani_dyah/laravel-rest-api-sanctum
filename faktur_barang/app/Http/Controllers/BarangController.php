<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBarang;
use App\Http\Requests\UpdateBarang;
use App\Http\Requests\ImportBarang;
use App\Models\Barang;
use App\Models\TipeBarang;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\UploadDump;
use Illuminate\Support\Facades\Validator;
class BarangController extends Controller
{
    CONST FETCHED_ATTRIBUTE = [
        "type_barang_id",
        "nama_barang",
        "harga",
        "gambar"
    ];

    public function index()
    {
        $barang = Barang::with(['tipe_barang'])->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Barang',
            'data'    => $barang
        ], 200);
    }
    public function show(Barang $barang)
    {
        $barang->load(['tipe_barang']);
        return response()->json([
            'success' => true,
            'message' => 'List Data Barang',
            'data'    => $barang
        ], 200);
    }
    public function store(StoreBarang $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        if($data['gambar']){
            UploadDump::where('rel_path', $data['gambar'])->update(array('link_status'=>1));
        }
        $barang = Barang::create($data);
        if($barang) {
            //jika data berhasil ditambah
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Ditambah',
                'data'    => $barang 
            ], 201);

        }else{
            //jika data gagal ditambah
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Ditambah',
            ], 409);
        }
    }
    public function update(UpdateBarang $request, Barang $barang)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        if($data['gambar'] != $barang->gambar){
            UploadDump::where('rel_path', $barang->gambar)->update(array('link_status'=>0));
            UploadDump::where('rel_path', $data['gambar'])->update(array('link_status'=>1));
        }
        $barang->update($data);
        if($barang) {
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Diupdate',
                'data'    => $barang  
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Diupdate!',
            ], 500);
        }
    }
    public function destroy(Barang $barang)
    {
        if($barang->gambar){
            UploadDump::where('rel_path', $barang->gambar)->update(array('link_status'=>0));
        }
        $barang->delete();
        if($barang){
            //data berhasil dihapus
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus',
            ], 200);
        }else{
            //data gagal dihapus
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Dihapus!',
            ], 500);
        }
    }
    public function destroyAll()
    {
        $barang = Barang::truncate();
        return response()->json([
            'success' => true,
            'message' => 'Data Berhasil Dihapus',
        ], 200);
    }
    public function import(ImportBarang $request)
    {
        $data = $request->data;
        $result = [];
        foreach ($data as $d) {
            $tipeBarang = TipeBarang::where('tipe_barang', $d['tipe_barang'])->first();
            $d['type_barang_id'] = $tipeBarang->id;
            if($d['gambar']){
                $uploadDump = UploadDump::where('rel_path', $d['gambar'])->first();
                if ($uploadDump) {
                    $uploadDump->update(array('link_status'=>1));
                }
            }
            $result[] = Barang::create($d);
        }
        return response($result);
    }
}
