<?php

namespace App\Http\Controllers;
use App\Http\Requests\StoreRegister;
use App\Http\Requests\UpdateRegister;
use App\Http\Requests\ImportBarang;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class RegisterController extends Controller
{
    CONST FETCHED_ATTRIBUTE = [
        "name",
        "email",
        "password"
    ];
    public function index()
    {
        $user = User::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data User',
            'data'    => $user 
        ], 200);
    }
    public function show(User $register)
    {
        return response()->json([
            'success' => true,
            'message' => 'Detail Data User',
            'data'    => $register
        ], 200);
    }
    public function store(StoreRegister $request)
    {
        $data = $request->only(self::FETCHED_ATTRIBUTE);
        $data['password']=Hash::make($request->password);
        $user = User::create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'password'  => Hash::make($data['password'])
        ]);
        if($user) {
            //jika data berhasil ditambah
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Ditambah',
                'data'    => $user
            ], 201);

        }else{
            //jika data gagal ditambah
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Ditambah',
            ], 409);
        }
    }
    // public function update(UpdateKasir $request, Kasir $kasir)
    // {
    //     $data = $request->only(self::FETCHED_ATTRIBUTE);
    //     if($data['foto'] != $kasir->foto){
    //         UploadDump::where('rel_path', $kasir->foto)->update(array('link_status'=>0));
    //         UploadDump::where('rel_path', $data['foto'])->update(array('link_status'=>1));
    //     }
    //     $kasir->update($data);
    //     if($kasir) {
    //         return response()->json([
    //             'success' => true,
    //             'message' => 'Data Berhasil Diupdate',
    //             'data'    => $kasir 
    //         ], 200);
    //     }else{
    //         return response()->json([
    //             'success' => false,
    //             'message' => 'Data Gagal Diupdate!',
    //         ], 500);
    //     }
    // }
    public function destroy(User $register)
    {
        $register->delete();
        if($register){
            //data berhasil dihapus
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil Dihapus',
            ], 200);
        }else{
            //data gagal dihapus
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal Dihapus!',
            ], 500);
        }
    }
    public function destroyAll()
    {
        $user = User::truncate();
        return response()->json([
            'success' => true,
            'message' => 'Data Berhasil Dihapus',
        ], 200);
    }
}
