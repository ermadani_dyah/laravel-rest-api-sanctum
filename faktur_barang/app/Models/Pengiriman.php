<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\PengirimianStatus;
use App\Models\Kurir;
use App\Models\Transaksi;
class Pengiriman extends Model
{
    use HasFactory;
    protected $table="pengirimen";
    protected $fillable = [
        'kurir_id','transaksi_id','pengiriman_status_id', 'tanggal_pengiriman'
    ]; 
    public function status(){
    	return $this->belongsTo(PengirimianStatus::class, "pengiriman_status_id");
    }
    public function kurir()
    {
        return $this->belongsTo(Kurir::class, "kurir_id");
    }
    public function transaksi()
    {
        return $this->belongsTo(Transaksi::class, "transaksi_id");
    }
}
