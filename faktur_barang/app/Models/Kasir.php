<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi;
class Kasir extends Model
{
    use HasFactory;
    protected $table="kasirs";
    protected $fillable = [
         'nama_kasir','foto'
    ]; 
    public function transaksis(){
    	return $this->hasMany(Transaksi::class, "kasir_id");
    }
}
