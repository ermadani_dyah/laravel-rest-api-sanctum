<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pengiriman;
class Kurir extends Model
{
    use HasFactory;
    protected $table="kurirs";
    protected $fillable = [
        'nama_kurir','logo'
    ]; 
    public function pengiriman_kurir(){
    	return $this->hasMany(Pengiriman::class,"kurir_id");
    }
}
