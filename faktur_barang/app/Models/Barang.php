<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi_detail;
use App\Models\TipeBarang;
class Barang extends Model
{
    use HasFactory;
    protected $table="barangs";
    protected $fillable = [
        'type_barang_id', 'nama_barang','harga','gambar'
    ];
    public function detail(){
    	return $this->hasMany(Transaksi_detail::class, "barang_id");
    }   
    public function tipe_barang(){
    	return $this->belongsTo(TipeBarang::class,"type_barang_id");
    }
}
