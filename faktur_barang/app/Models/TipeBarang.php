<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Barang;
class TipeBarang extends Model
{
    use HasFactory;
    protected $table="tipe_barangs";
    protected $fillable = [
        'tipe_barang'
    ];
    public function barang(){
    	return $this->hasMany(Barang::class,"type_barang_id");
    } 
}
