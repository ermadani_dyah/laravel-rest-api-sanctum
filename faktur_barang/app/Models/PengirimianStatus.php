<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pengiriman;
class PengirimianStatus extends Model
{
    use HasFactory;
    protected $table="pengirimian_statuses";
    protected $fillable = [
        'id','nama_status'
    ]; 
    public $incrementing =false;
    public function pengiriman()
    {
        return $this->hasMany(Pengiriman::class, "pengiriman_status_id");
    }
}
