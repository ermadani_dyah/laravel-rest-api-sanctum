<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UploadDump extends Model
{
    use HasFactory;

    protected $table = 'upload_dump';
    protected $fillable = ['file_name','file_type','file_siza','category','folder','uploader_ip',
    'rel_path','abs_path','upload_status','link_status'];
}
