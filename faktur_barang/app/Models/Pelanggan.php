<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Transaksi;
class Pelanggan extends Model
{
    use HasFactory;
    protected $table="pelanggans";
    protected $fillable = [
        'nama_pelanggan','photo'
    ]; 
    public function transaksis(){
    	return $this->hasMany(Transaksi::class, "pelanggan_id");
    }
}
