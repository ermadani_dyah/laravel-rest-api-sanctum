<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\TransaksiDetail;
use App\Models\Pelanggan;
use App\Models\Kasir;
use App\Models\Pengiriman;
class Transaksi extends Model
{
    use HasFactory;
    protected $table="transaksis";
    public function pelanggans()
    {
        return $this->belongsTo(Pelanggan::class,"pelanggan_id");
    }
    public function kasirs()
    {
        return $this->belongsTo(Kasir::class,"kasir_id");
    }
    public function details(){
    	return $this->hasMany(TransaksiDetail::class, "transaksi_id");
    }
    public function pengirimans(){
    	return $this->belongsTo(Pengiriman::class, "transaksi_id");
    }
    protected $fillable = [
        'kasir_id','pelanggan_id','total', 'tanggal_transaksi'
    ]; 
    
}
