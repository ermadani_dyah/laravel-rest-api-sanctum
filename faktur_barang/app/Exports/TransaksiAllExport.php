<?php

namespace App\Exports;

use App\Models\Transaksi;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
class TransaksiAllExport implements FromCollection,WithHeadings,WithMapping,ShouldAutoSize
{
    
    public function collection()
    {
        $transaksi = Transaksi::with(['details.barangs','kasirs','pelanggans'])->get();
        return $transaksi;
    }
    public function map($transaksi): array
    {  
        $result=
            [
                [
                    $transaksi->id,
                    $transaksi->tanggal_transaksi,
                    $transaksi->kasirs->nama_kasir,
                    $transaksi->pelanggans->nama_pelanggan,
                ]
            ];
            foreach($transaksi->details as $item){
                $result[] = [
                    "","","","",
                    $item->barangs->nama_barang,
                    $item->barangs->harga,
                    $item->kuantitas,
                    $transaksi->total
                ];
            }
        return $result;
    }
    public function headings(): array
    {
        return [
            'id',
            'Tanggal Transaksi',
            'Kasir',
            'Pelanggan',
            'Barang',
            'Harga',
            'Kuantitas',
            'Total'
        ];
    }
}
