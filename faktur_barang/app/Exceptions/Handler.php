<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
        // route
        $this->renderable(function (HttpException $e, $request)
        {
            return response([
                "message" => $e->getMessage(),
                "errors" => null
            ], $e->getStatusCode());
        });
        // validasi
        $this->renderable(function (ValidationException $e, $request) {
            return response([
                "message" => $e->getMessage(),
                "errors" => $e->errors()
            ], 422);
        });
        // token route
        $this->renderable(function (RouteNotFoundException $e, $request)
        {
            return response([
                "message" => "Unauthorized",
                "errors" => null
            ], 401);
        });
    }
}
