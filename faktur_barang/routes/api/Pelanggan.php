<?php

use Illuminate\Support\Facades\Route;

Route::name('pelanggan.')->prefix('pelanggan')->group(function () {
    Route::post('/pelanggan-import', 'PelangganController@import')->name('pelanggan-import')->middleware('auth:sanctum');
    Route::delete('/destroyAll', 'PelangganController@destroyAll')->name('destroy-all')->middleware('auth:sanctum');
    Route::post('/{pelanggan}', 'PelangganController@update')->name('update-post')->middleware('auth:sanctum');
});
Route::apiResource('pelanggan', 'PelangganController')->middleware('auth:sanctum');
