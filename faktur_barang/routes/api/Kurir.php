<?php

use Illuminate\Support\Facades\Route;

Route::name('kurir.')->prefix('kurir')->group(function () {
    Route::post('/kurir-import', 'KurirController@import')->name('kurir-import')->middleware('auth:sanctum');
    Route::delete('/destroyAll', 'KurirController@destroyAll')->name('destroy-all')->middleware('auth:sanctum');
    Route::post('/{kurir}', 'KurirController@update')->name('update-post')->middleware('auth:sanctum');
});
Route::apiResource('kurir', 'KurirController')->middleware('auth:sanctum');
