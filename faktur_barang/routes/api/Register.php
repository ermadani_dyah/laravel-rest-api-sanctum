<?php

use Illuminate\Support\Facades\Route;

Route::name('register.')->prefix('register')->group(function () {
    Route::delete('/destroyAll', 'RegisterController@destroyAll')->name('destroy-all');
});
Route::apiResource('register', 'RegisterController');
