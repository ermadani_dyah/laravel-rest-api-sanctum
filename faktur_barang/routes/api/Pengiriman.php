<?php

use Illuminate\Support\Facades\Route;

Route::name('pengiriman.')->prefix('pengiriman')->group(function () {
    Route::post('/pengiriman-import', 'PengirimanController@import')->name('pengiriman-import')->middleware('auth:sanctum');
    Route::delete('/destroyAll', 'PengirimanController@destroyAll')->name('destroy-all')->middleware('auth:sanctum');
});
Route::apiResource('pengiriman', 'PengirimanController')->middleware('auth:sanctum');
