<?php

use Illuminate\Support\Facades\Route;

Route::name('barang.')->prefix('barang')->group(function () {
    Route::post('/barang-import', 'BarangController@import')->name('barang-import')->middleware('auth:sanctum');
    Route::delete('/destroyAll', 'BarangController@destroyAll')->name('destroy-all')->middleware('auth:sanctum');
    Route::post('/{barang}', 'BarangController@update')->name('update-post')->middleware('auth:sanctum');
});
Route::apiResource('barang', 'BarangController')->middleware('auth:sanctum');
