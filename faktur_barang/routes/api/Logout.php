<?php

use Illuminate\Support\Facades\Route;

Route::name('logout.')->prefix('logout')->group(function () {
    Route::post('/logout', 'LoginController@logout')->name('logout')->middleware('auth:sanctum');
});
