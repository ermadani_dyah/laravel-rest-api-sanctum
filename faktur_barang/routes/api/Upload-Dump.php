<?php

use Illuminate\Support\Facades\Route;

Route::name('upload-dumps.')->prefix('upload-dumps')->group(function () {
    // Route::delete('/destroyAll', 'BarangController@destroyAll')->name('destroy-all');
    // Route::post('/{barang}', 'BarangController@update')->name('update-post');
});
Route::apiResource('upload-dumps', 'UploadDumpController')->middleware('auth:sanctum');
