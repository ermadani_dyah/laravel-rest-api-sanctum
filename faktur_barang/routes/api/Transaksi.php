<?php

use Illuminate\Support\Facades\Route;

Route::name('transaksi.')->prefix('transaksi')->group(function () {
    Route::post('/transaksi-import', 'TransaksiController@import')->name('transaksi-import')->middleware('auth:sanctum');
    Route::delete('/destroyAll', 'TransaksiController@destroyAll')->name('destroy-all')->middleware('auth:sanctum');
    Route::get('/transaksi-export-pdf/{transaksi}', 'TransaksiController@exportPdf')->name('transaksi-export-pdf')->middleware('auth:sanctum');
    Route::get('/transaksi-export-excel', 'TransaksiController@exportExcel')->name('transaksi-export-excel')->middleware('auth:sanctum');
    Route::post('/transaksi-export-excel-post', 'TransaksiController@exportToExcel')->name('transaksi-export-excel-post')->middleware('auth:sanctum');
});
Route::apiResource('transaksi', 'TransaksiController')->middleware('auth:sanctum');
