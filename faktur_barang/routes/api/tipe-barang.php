<?php

use Illuminate\Support\Facades\Route;

Route::name('tipe-barang.')->prefix('tipe-barang')->group(function () {
    Route::post('/tipe-barang-import', 'TipeBarangController@import')->name('tipe-barang-import')->middleware('auth:sanctum');
    Route::delete('/destroyAll', 'TipeBarangController@destroyAll')->name('destroy-all')->middleware('auth:sanctum');
});
Route::apiResource('tipe-barang', 'TipeBarangController')->middleware('auth:sanctum');
