<?php

use Illuminate\Support\Facades\Route;

Route::name('login.')->prefix('login')->group(function () {
    Route::post('/login', 'LoginController@index')->name('login');
});
