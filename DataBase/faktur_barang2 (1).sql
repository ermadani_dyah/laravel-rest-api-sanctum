-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 30, 2021 at 12:20 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `faktur_barang2`
--

-- --------------------------------------------------------

--
-- Table structure for table `barangs`
--

CREATE TABLE `barangs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_barang_id` bigint(20) UNSIGNED NOT NULL,
  `nama_barang` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `barangs`
--

INSERT INTO `barangs` (`id`, `type_barang_id`, `nama_barang`, `harga`, `created_at`, `updated_at`, `gambar`) VALUES
(2, 3, 'Pisau', 3000, '2021-07-12 05:17:35', '2021-07-19 01:30:16', 'storage/upload/barang/1626683416_7a9c61011ca896602844e89931be986a.png'),
(3, 3, 'Piring', 34000, '2021-07-12 20:47:10', '2021-07-21 08:17:05', 'dump/kurir/1626876468_7a9c61011ca896602844e89931be986a.png'),
(4, 3, 'piring', 3000, '2021-07-18 21:13:46', '2021-07-18 21:13:46', ''),
(5, 2, 'Pisau', 50000, '2021-07-19 01:24:12', '2021-07-19 01:24:12', 'storage/upload/barang/1626683052_9a9c2c191f373232.png'),
(7, 7, 'Sabun', 10000, '2021-07-26 08:07:03', '2021-07-26 08:07:03', 'dump/Barang/1626880997_unnamed-(2).png');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kasirs`
--

CREATE TABLE `kasirs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kasir` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kasirs`
--

INSERT INTO `kasirs` (`id`, `nama_kasir`, `created_at`, `updated_at`, `foto`) VALUES
(2, 'Norman', '2021-07-12 06:58:14', '2021-07-12 06:58:14', ''),
(5, 'Jaemin', '2021-07-16 07:16:11', '2021-07-19 01:39:49', 'storage/upload/kasir/1626683989_ikonxmas.png'),
(6, 'BI', '2021-07-19 01:36:16', '2021-07-19 01:36:16', 'storage/upload/kasir/1626683776_unnamed.png'),
(7, 'Joni', NULL, NULL, 'storage/upload/kasir/1626683989_ikonxmas.png'),
(8, 'Cengkir Saadat Budiman', NULL, NULL, '8c808d536ff2c3a306caac8cdfc7f7e0.png'),
(9, 'Ratna Suci Mulyani S.Ked', NULL, NULL, '4a89754275e92c59b719d10b6c14071e.png'),
(10, 'Luhung Saptono', NULL, NULL, 'ee6705f4c0c779deadfbfabb5b0c815b.png'),
(11, 'Kasiran Prasasta S.Pd', NULL, NULL, '1f566e1a161ab80a0841742318e7d0ec.png'),
(12, 'Baktianto Kusumo', NULL, NULL, 'a546fe1982b46531b7ad27bc549a2669.png'),
(13, 'dyah', '2021-07-26 06:31:06', '2021-07-26 06:31:06', 'dump/Kasir/1626877826_2f1ed4340dfe8cf40ede642ca03f783c.png');

-- --------------------------------------------------------

--
-- Table structure for table `kurirs`
--

CREATE TABLE `kurirs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kurir` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kurirs`
--

INSERT INTO `kurirs` (`id`, `nama_kurir`, `created_at`, `updated_at`, `logo`) VALUES
(2, 'J&T', '2021-07-12 07:09:12', '2021-07-12 07:12:37', ''),
(3, 'Si cepat', '2021-07-12 07:09:28', '2021-07-12 07:09:28', ''),
(6, 'JNT', '2021-07-19 00:38:29', '2021-07-19 00:38:29', 'storage/upload/kurir/1626680309_Cloud-Database-sql-server-2014-300x216.jpg'),
(8, 'J&T', '2021-07-21 08:01:58', '2021-07-21 08:01:58', 'dump/Kasir/1626877605_D5O3c-tUcAU345T-680x384.png'),
(9, 'Antar Raja', '2021-07-26 07:43:07', '2021-07-26 07:43:07', 'dump/Pelanggan/1626880954_unnamed-(3).png');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_07_12_072751_create_tipe_barangs_table', 1),
(5, '2021_07_12_072831_create_barangs_table', 1),
(6, '2021_07_12_072856_create_pelanggans_table', 1),
(7, '2021_07_12_072923_create_kurirs_table', 1),
(8, '2021_07_12_072956_create_pengirimian_statuses_table', 1),
(9, '2021_07_12_073014_create_kasirs_table', 1),
(10, '2021_07_12_073049_create_transaksis_table', 1),
(11, '2021_07_12_073120_create_transaksi_details_table', 1),
(12, '2021_07_12_073141_create_pengirimen_table', 2),
(13, '2021_07_12_084121_add_pengiriman_status_id_to_pengirimen_table', 3),
(14, '2021_07_12_084504_add_pengiriman_status_to_pengirimen_table', 4),
(15, '2021_07_12_085431_add_drop_colom_id_to_pengirimian_statuses_table', 5),
(16, '2021_07_12_085612_add_colom_id_to_pengirimen_table', 5),
(17, '2021_07_12_090151_add_colom_name_id_to_pengirimian_statuses_table', 6),
(18, '2021_07_12_090427_add_forgent_key_to_pengirimen_table', 7),
(19, '2021_07_12_090748_add_colom_name_forgent_key_to_pengirimen_table', 8),
(20, '2021_07_12_091819_add_primary_key_to_pengirimian_statuses_table', 9),
(21, '2021_07_12_091905_add_forgent_key_name_id_to_pengirimen_table', 9),
(22, '2021_07_19_073016_add_logo_to_kurirs_table', 10),
(23, '2021_07_19_073331_add_foto_to_kasirs_table', 11),
(24, '2021_07_19_073416_add_gambar_to_barangs_table', 11),
(25, '2021_07_19_073517_add_photo_to_pelanggans_table', 11),
(26, '2021_07_21_131623_create_upload_dump_table', 12),
(27, '2016_06_01_000001_create_oauth_auth_codes_table', 13),
(28, '2016_06_01_000002_create_oauth_access_tokens_table', 13),
(29, '2016_06_01_000003_create_oauth_refresh_tokens_table', 13),
(30, '2016_06_01_000004_create_oauth_clients_table', 13),
(31, '2016_06_01_000005_create_oauth_personal_access_clients_table', 13),
(32, '2021_07_28_101456_create_user2s_table', 14),
(33, '2019_12_14_000001_create_personal_access_tokens_table', 15);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pelanggans`
--

CREATE TABLE `pelanggans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_pelanggan` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pelanggans`
--

INSERT INTO `pelanggans` (`id`, `nama_pelanggan`, `created_at`, `updated_at`, `photo`) VALUES
(2, 'Dyah', '2021-07-12 07:26:43', '2021-07-16 04:01:24', ''),
(3, 'Jaemin', '2021-07-16 03:59:14', '2021-07-16 03:59:14', ''),
(5, 'Mark', '2021-07-16 03:59:29', '2021-07-16 03:59:29', ''),
(6, 'Haechan', '2021-07-16 07:18:04', '2021-07-16 07:18:04', ''),
(9, 'saya', '2021-07-19 01:03:18', '2021-07-19 01:07:38', 'storage/upload/pelanggan/1626682058_D5O3c-tUcAU345T-680x384.png'),
(10, 'Jaemin', '2021-07-21 08:24:26', '2021-07-21 08:24:26', 'dump/Pelanggan/1626880937_unnamed.png'),
(11, 'Jeno', '2021-07-26 07:46:03', '2021-07-26 07:46:03', 'dump/Barang/1626880986_unnamed-(3).png');

-- --------------------------------------------------------

--
-- Table structure for table `pengirimen`
--

CREATE TABLE `pengirimen` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kurir_id` bigint(20) UNSIGNED NOT NULL,
  `transaksi_id` bigint(20) UNSIGNED NOT NULL,
  `tanggal_pengiriman` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pengiriman_status_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengirimen`
--

INSERT INTO `pengirimen` (`id`, `kurir_id`, `transaksi_id`, `tanggal_pengiriman`, `created_at`, `updated_at`, `pengiriman_status_id`) VALUES
(3, 3, 5, '2021-09-30', '2021-07-13 08:40:45', '2021-07-18 21:18:54', 'A'),
(4, 2, 5, '2021-07-09', '2021-07-13 08:50:15', '2021-07-13 08:50:15', 'R'),
(7, 3, 5, '2021-07-30', '2021-07-18 21:17:55', '2021-07-18 21:17:55', 'A'),
(8, 9, 8, '2021-09-30', '2021-07-26 08:26:03', '2021-07-26 08:26:03', 'B');

-- --------------------------------------------------------

--
-- Table structure for table `pengirimian_statuses`
--

CREATE TABLE `pengirimian_statuses` (
  `nama_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengirimian_statuses`
--

INSERT INTO `pengirimian_statuses` (`nama_status`, `created_at`, `updated_at`, `id`) VALUES
('Jaemin', NULL, '2021-07-12 09:14:40', 'A'),
('Belum Dipacking', NULL, NULL, 'B'),
('Dyah', '2021-07-16 04:16:08', '2021-07-16 04:16:52', 'L'),
('BB', '2021-07-12 09:13:23', '2021-07-12 09:13:23', 'R'),
('Sampai', '2021-07-26 07:52:24', '2021-07-26 07:52:24', 'w');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'ApiToken', 'e07736bcf31be556568781b2f944a114f828864bb72cdd317edf420ad450542a', '[\"*\"]', NULL, '2021-07-29 20:03:48', '2021-07-29 20:03:48'),
(2, 'App\\Models\\User', 1, 'ApiToken', '06c610672ee4c538c3dffe555f5d86d2d97697ce25d58b529c99dc8c90de0c83', '[\"*\"]', NULL, '2021-07-29 20:07:21', '2021-07-29 20:07:21'),
(3, 'App\\Models\\User', 1, 'ApiToken', '0545749c379cffdaea1ac8f79de4054e231f139ace00103e93977c29879e38bf', '[\"*\"]', NULL, '2021-07-29 20:19:12', '2021-07-29 20:19:12');

-- --------------------------------------------------------

--
-- Table structure for table `tipe_barangs`
--

CREATE TABLE `tipe_barangs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipe_barang` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipe_barangs`
--

INSERT INTO `tipe_barangs` (`id`, `tipe_barang`, `created_at`, `updated_at`) VALUES
(2, 'Alat Masak', '2021-07-12 03:17:45', '2021-07-12 03:17:45'),
(3, 'Alat Makan', '2021-07-12 03:30:43', '2021-07-12 03:30:43'),
(7, 'Alat Mandi', '2021-07-26 07:39:48', '2021-07-26 07:39:48'),
(8, 'Alat Sholat', '2021-07-26 07:39:48', '2021-07-26 07:39:48'),
(9, 'Alat Dapur', '2021-07-29 04:42:36', '2021-07-29 04:42:36'),
(10, 'Alat Rumah tanggal', '2021-07-29 04:42:36', '2021-07-29 04:42:36');

-- --------------------------------------------------------

--
-- Table structure for table `transaksis`
--

CREATE TABLE `transaksis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `total` bigint(20) NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `kasir_id` bigint(20) UNSIGNED NOT NULL,
  `pelanggan_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksis`
--

INSERT INTO `transaksis` (`id`, `total`, `tanggal_transaksi`, `kasir_id`, `pelanggan_id`, `created_at`, `updated_at`) VALUES
(1, 3000, '2021-07-13', 2, 2, '2021-07-12 22:33:27', '2021-07-12 22:33:27'),
(2, 3000, '2021-07-13', 2, 2, '2021-07-12 22:34:15', '2021-07-12 22:34:15'),
(5, 3000, '2021-07-25', 2, 2, '2021-07-12 22:59:41', '2021-07-18 21:11:10'),
(6, 3000, '2021-07-17', 2, 2, '2021-07-12 23:43:21', '2021-07-12 23:43:21'),
(7, 3000, '2021-07-15', 2, 2, '2021-07-13 05:40:45', '2021-07-13 05:40:45'),
(8, 3000, '2021-07-15', 2, 2, '2021-07-13 05:42:36', '2021-07-13 05:42:36'),
(9, 3000, '2021-07-17', 2, 2, '2021-07-16 05:34:54', '2021-07-16 05:34:54'),
(10, 3000, '2021-07-17', 2, 2, '2021-07-16 05:38:21', '2021-07-16 05:38:21'),
(11, 3000, '2021-07-17', 2, 2, '2021-07-16 05:39:18', '2021-07-16 05:39:18'),
(13, 3000, '2021-07-15', 2, 2, '2021-07-16 05:46:43', '2021-07-16 05:46:43'),
(14, 3000, '2021-07-15', 2, 2, '2021-07-16 07:43:53', '2021-07-16 07:43:53'),
(16, 3000, '2021-07-15', 12, 3, '2021-07-26 08:55:03', '2021-07-26 08:55:04'),
(17, 3000, '2021-07-15', 12, 3, '2021-07-26 08:56:07', '2021-07-26 08:56:07'),
(18, 3000, '2021-07-15', 12, 3, '2021-07-26 08:59:45', '2021-07-26 08:59:45'),
(19, 3000, '2021-07-15', 12, 3, '2021-07-26 09:00:44', '2021-07-26 09:00:44'),
(20, 3000, '2021-07-15', 12, 3, '2021-07-26 09:01:44', '2021-07-26 09:01:44'),
(21, 3000, '2021-07-15', 12, 3, '2021-07-26 09:01:54', '2021-07-26 09:01:54'),
(22, 3000, '2021-07-15', 12, 3, '2021-07-26 09:02:23', '2021-07-26 09:02:23'),
(23, 3000, '2021-07-15', 12, 3, '2021-07-26 09:02:50', '2021-07-26 09:02:50'),
(24, 3000, '2021-07-15', 12, 3, '2021-07-26 09:03:31', '2021-07-26 09:03:31'),
(25, 3000, '2021-07-15', 12, 3, '2021-07-26 09:04:09', '2021-07-26 09:04:09'),
(26, 3000, '2021-07-15', 12, 3, '2021-07-26 09:05:18', '2021-07-26 09:05:18'),
(27, 3000, '2021-07-15', 12, 3, '2021-07-26 09:05:46', '2021-07-26 09:05:46'),
(28, 3000, '2021-07-15', 12, 3, '2021-07-26 09:07:06', '2021-07-26 09:07:06'),
(29, 3000, '2021-07-15', 12, 3, '2021-07-26 09:07:49', '2021-07-26 09:07:49'),
(30, 3000, '2021-07-15', 12, 3, '2021-07-26 09:10:23', '2021-07-26 09:10:23'),
(31, 3000, '2021-07-15', 12, 3, '2021-07-26 09:10:41', '2021-07-26 09:10:41'),
(32, 3000, '2021-07-15', 12, 3, '2021-07-26 09:11:07', '2021-07-26 09:11:07'),
(33, 3000, '2021-07-15', 12, 3, '2021-07-26 09:12:16', '2021-07-26 09:12:16'),
(34, 3000, '2021-07-15', 12, 3, '2021-07-26 09:14:42', '2021-07-26 09:14:42'),
(35, 3000, '2021-07-15', 12, 3, '2021-07-26 09:16:06', '2021-07-26 09:16:06'),
(36, 3000, '2021-07-15', 12, 3, '2021-07-26 09:16:42', '2021-07-26 09:16:42'),
(37, 3000, '2021-07-15', 12, 3, '2021-07-26 09:16:54', '2021-07-26 09:16:54'),
(38, 3000, '2021-07-15', 12, 3, '2021-07-26 09:17:35', '2021-07-26 09:17:35'),
(39, 3000, '2021-07-15', 12, 3, '2021-07-26 09:18:43', '2021-07-26 09:18:43'),
(40, 3000, '2021-07-15', 12, 3, '2021-07-26 09:20:45', '2021-07-26 09:20:45'),
(41, 3000, '2021-07-15', 12, 3, '2021-07-26 09:21:21', '2021-07-26 09:21:21'),
(42, 3000, '2021-07-15', 12, 3, '2021-07-26 18:26:14', '2021-07-26 18:26:14'),
(43, 3000, '2021-07-15', 12, 3, '2021-07-26 18:27:15', '2021-07-26 18:27:15'),
(44, 3000, '2021-07-15', 12, 3, '2021-07-26 18:27:32', '2021-07-26 18:27:32'),
(45, 3000, '2021-07-15', 12, 3, '2021-07-26 18:28:53', '2021-07-26 18:28:53'),
(46, 3000, '2021-07-15', 12, 3, '2021-07-26 18:29:30', '2021-07-26 18:29:30'),
(47, 3000, '2021-07-15', 12, 3, '2021-07-26 18:30:03', '2021-07-26 18:30:03'),
(48, 3000, '2021-07-15', 12, 3, '2021-07-26 18:30:50', '2021-07-26 18:30:50'),
(49, 3000, '2021-07-15', 12, 3, '2021-07-26 18:31:13', '2021-07-26 18:31:13'),
(50, 3000, '2021-07-15', 12, 3, '2021-07-26 18:32:21', '2021-07-26 18:32:21'),
(51, 3000, '2021-07-15', 12, 3, '2021-07-26 18:33:58', '2021-07-26 18:33:58'),
(52, 3000, '2021-07-15', 12, 3, '2021-07-26 18:37:14', '2021-07-26 18:37:14'),
(53, 3000, '2021-07-15', 12, 3, '2021-07-26 18:37:55', '2021-07-26 18:37:55'),
(54, 3000, '2021-07-15', 12, 3, '2021-07-26 18:38:54', '2021-07-26 18:38:54'),
(55, 3000, '2021-07-15', 12, 3, '2021-07-26 18:39:38', '2021-07-26 18:39:38'),
(56, 3000, '2021-07-15', 12, 3, '2021-07-26 18:41:36', '2021-07-26 18:41:36'),
(57, 3000, '2021-07-15', 12, 3, '2021-07-26 18:43:01', '2021-07-26 18:43:01'),
(58, 3000, '2021-07-15', 2, 2, '2021-07-26 19:39:22', '2021-07-26 19:39:22'),
(59, 3000, '2021-07-15', 12, 3, '2021-07-26 23:16:27', '2021-07-26 23:16:27'),
(60, 3000, '2021-07-15', 12, 3, '2021-07-26 23:16:37', '2021-07-26 23:16:37'),
(61, 3000, '2021-07-15', 12, 3, '2021-07-26 23:23:12', '2021-07-26 23:23:12'),
(62, 3000, '2021-07-15', 12, 3, '2021-07-26 23:24:13', '2021-07-26 23:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_details`
--

CREATE TABLE `transaksi_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kuantitas` int(11) NOT NULL,
  `barang_id` bigint(20) UNSIGNED NOT NULL,
  `transaksi_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksi_details`
--

INSERT INTO `transaksi_details` (`id`, `kuantitas`, `barang_id`, `transaksi_id`, `created_at`, `updated_at`) VALUES
(3, 1, 3, 6, '2021-07-12 23:43:21', '2021-07-12 23:43:21'),
(7, 1, 3, 7, '2021-07-13 05:40:45', '2021-07-13 05:40:45'),
(8, 1, 2, 8, '2021-07-13 05:42:36', '2021-07-13 05:42:36'),
(11, 3, 2, 13, '2021-07-16 05:46:43', '2021-07-16 05:46:43'),
(16, 3, 2, 5, '2021-07-18 21:11:10', '2021-07-18 21:11:10'),
(17, 3, 2, 16, '2021-07-26 08:55:03', '2021-07-26 08:55:03'),
(18, 3, 2, 41, '2021-07-26 09:21:22', '2021-07-26 09:21:22'),
(19, 3, 2, 42, '2021-07-26 18:26:14', '2021-07-26 18:26:14'),
(20, 3, 2, 43, '2021-07-26 18:27:16', '2021-07-26 18:27:16'),
(21, 3, 2, 44, '2021-07-26 18:27:32', '2021-07-26 18:27:32'),
(22, 3, 2, 45, '2021-07-26 18:28:53', '2021-07-26 18:28:53'),
(23, 3, 2, 46, '2021-07-26 18:29:30', '2021-07-26 18:29:30'),
(24, 3, 2, 47, '2021-07-26 18:30:03', '2021-07-26 18:30:03'),
(25, 3, 2, 48, '2021-07-26 18:30:50', '2021-07-26 18:30:50'),
(26, 1, 2, 49, '2021-07-26 18:31:13', '2021-07-26 18:31:13'),
(27, 1, 2, 50, '2021-07-26 18:32:22', '2021-07-26 18:32:22'),
(28, 1, 2, 51, '2021-07-26 18:33:58', '2021-07-26 18:33:58'),
(29, 1, 2, 52, '2021-07-26 18:37:14', '2021-07-26 18:37:14'),
(30, 1, 2, 53, '2021-07-26 18:37:55', '2021-07-26 18:37:55'),
(31, 1, 2, 54, '2021-07-26 18:38:54', '2021-07-26 18:38:54'),
(32, 1, 2, 55, '2021-07-26 18:39:38', '2021-07-26 18:39:38'),
(33, 1, 2, 56, '2021-07-26 18:41:36', '2021-07-26 18:41:36'),
(34, 1, 2, 57, '2021-07-26 18:43:01', '2021-07-26 18:43:01'),
(35, 2, 7, 58, '2021-07-26 19:39:22', '2021-07-26 19:39:22'),
(36, 1, 2, 59, '2021-07-26 23:16:27', '2021-07-26 23:16:27'),
(37, 1, 2, 60, '2021-07-26 23:16:37', '2021-07-26 23:16:37'),
(38, 1, 2, 61, '2021-07-26 23:23:12', '2021-07-26 23:23:12'),
(39, 1, 2, 62, '2021-07-26 23:24:13', '2021-07-26 23:24:13');

-- --------------------------------------------------------

--
-- Table structure for table `upload_dump`
--

CREATE TABLE `upload_dump` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_siza` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uploader_ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rel_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abs_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `upload_status` tinyint(4) NOT NULL DEFAULT 1,
  `link_status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `upload_dump`
--

INSERT INTO `upload_dump` (`id`, `file_name`, `file_type`, `file_siza`, `category`, `folder`, `uploader_ip`, `rel_path`, `abs_path`, `upload_status`, `link_status`, `created_at`, `updated_at`) VALUES
(1, '7a9c61011ca896602844e89931be986a.png', 'png', 41943040, 'Gambar', 'kurir', '127.0.0.1', 'dump/kurir/1626876468_7a9c61011ca896602844e89931be986a.png', 'http://localhost:8000/dump/kurir/1626876468_7a9c61011ca896602844e89931be986a.png', 1, 1, '2021-07-21 07:07:48', '2021-07-21 08:17:05'),
(2, 'D5O3c-tUcAU345T-680x384.png', 'png', 41943040, 'Gambar', 'Kasir', '127.0.0.1', 'dump/Kasir/1626877605_D5O3c-tUcAU345T-680x384.png', 'http://localhost:8000/dump/Kasir/1626877605_D5O3c-tUcAU345T-680x384.png', 1, 1, '2021-07-21 07:26:45', '2021-07-21 08:01:58'),
(3, '2f1ed4340dfe8cf40ede642ca03f783c.png', 'png', 41943040, 'Gambar', 'Kasir', '127.0.0.1', 'dump/Kasir/1626877826_2f1ed4340dfe8cf40ede642ca03f783c.png', 'http://localhost:8000/dump/Kasir/1626877826_2f1ed4340dfe8cf40ede642ca03f783c.png', 1, 1, '2021-07-21 07:30:26', '2021-07-26 06:31:06'),
(4, 'unnamed.png', 'png', 41943040, 'Gambar', 'Pelanggan', '127.0.0.1', 'dump/Pelanggan/1626880937_unnamed.png', 'http://localhost:8000/dump/Pelanggan/1626880937_unnamed.png', 1, 1, '2021-07-21 08:22:17', '2021-07-21 08:24:26'),
(5, 'unnamed-(3).png', 'png', 41943040, 'Gambar', 'Pelanggan', '127.0.0.1', 'dump/Pelanggan/1626880954_unnamed-(3).png', 'http://localhost:8000/dump/Pelanggan/1626880954_unnamed-(3).png', 1, 1, '2021-07-21 08:22:34', '2021-07-26 07:43:07'),
(6, 'unnamed-(3).png', 'png', 41943040, 'Gambar', 'Barang', '127.0.0.1', 'dump/Barang/1626880986_unnamed-(3).png', 'http://localhost:8000/dump/Barang/1626880986_unnamed-(3).png', 1, 1, '2021-07-21 08:23:06', '2021-07-26 07:46:03'),
(7, 'unnamed-(2).png', 'png', 41943040, 'Gambar', 'Barang', '127.0.0.1', 'dump/Barang/1626880997_unnamed-(2).png', 'http://localhost:8000/dump/Barang/1626880997_unnamed-(2).png', 1, 1, '2021-07-21 08:23:17', '2021-07-26 08:02:59');

-- --------------------------------------------------------

--
-- Table structure for table `user2s`
--

CREATE TABLE `user2s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user2s`
--

INSERT INTO `user2s` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Fika Ridaul Maulayya', 'admin@gmail.com', NULL, '$2y$10$Juf.o0ZlznhvGyKtrShGJ.ACnu5btzZDO1i8TUks9mahdq1yNLoDy', NULL, '2021-07-28 06:15:32', '2021-07-28 06:15:32'),
(2, 'Ermadani Dyah R', 'ermadani.dyah09@gmail.com', NULL, '$2y$10$XJrZ4m2pBmETICm0KoQrjeC/ZDtDDaYVRxmQvojBAQLKdKyEEk2J.', NULL, '2021-07-29 20:43:04', '2021-07-29 20:43:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barangs_type_barang_id_foreign` (`type_barang_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `kasirs`
--
ALTER TABLE `kasirs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kurirs`
--
ALTER TABLE `kurirs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pelanggans`
--
ALTER TABLE `pelanggans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengirimen`
--
ALTER TABLE `pengirimen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pengirimen_kurir_id_foreign` (`kurir_id`),
  ADD KEY `pengirimen_transaksi_id_foreign` (`transaksi_id`),
  ADD KEY `pengirimen_pengiriman_status_id_foreign` (`pengiriman_status_id`);

--
-- Indexes for table `pengirimian_statuses`
--
ALTER TABLE `pengirimian_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `tipe_barangs`
--
ALTER TABLE `tipe_barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksis`
--
ALTER TABLE `transaksis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksis_kasir_id_foreign` (`kasir_id`),
  ADD KEY `transaksis_pelanggan_id_foreign` (`pelanggan_id`);

--
-- Indexes for table `transaksi_details`
--
ALTER TABLE `transaksi_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksi_details_transaksi_id_foreign` (`transaksi_id`),
  ADD KEY `transaksi_details_barang_id_foreign` (`barang_id`);

--
-- Indexes for table `upload_dump`
--
ALTER TABLE `upload_dump`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user2s`
--
ALTER TABLE `user2s`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user2s_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasirs`
--
ALTER TABLE `kasirs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `kurirs`
--
ALTER TABLE `kurirs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `pelanggans`
--
ALTER TABLE `pelanggans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pengirimen`
--
ALTER TABLE `pengirimen`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tipe_barangs`
--
ALTER TABLE `tipe_barangs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `transaksis`
--
ALTER TABLE `transaksis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `transaksi_details`
--
ALTER TABLE `transaksi_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `upload_dump`
--
ALTER TABLE `upload_dump`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user2s`
--
ALTER TABLE `user2s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barangs`
--
ALTER TABLE `barangs`
  ADD CONSTRAINT `barangs_type_barang_id_foreign` FOREIGN KEY (`type_barang_id`) REFERENCES `tipe_barangs` (`id`);

--
-- Constraints for table `pengirimen`
--
ALTER TABLE `pengirimen`
  ADD CONSTRAINT `pengirimen_kurir_id_foreign` FOREIGN KEY (`kurir_id`) REFERENCES `kurirs` (`id`),
  ADD CONSTRAINT `pengirimen_pengiriman_status_id_foreign` FOREIGN KEY (`pengiriman_status_id`) REFERENCES `pengirimian_statuses` (`id`),
  ADD CONSTRAINT `pengirimen_transaksi_id_foreign` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksis` (`id`);

--
-- Constraints for table `transaksis`
--
ALTER TABLE `transaksis`
  ADD CONSTRAINT `transaksis_kasir_id_foreign` FOREIGN KEY (`kasir_id`) REFERENCES `kasirs` (`id`),
  ADD CONSTRAINT `transaksis_pelanggan_id_foreign` FOREIGN KEY (`pelanggan_id`) REFERENCES `pelanggans` (`id`);

--
-- Constraints for table `transaksi_details`
--
ALTER TABLE `transaksi_details`
  ADD CONSTRAINT `transaksi_details_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`),
  ADD CONSTRAINT `transaksi_details_transaksi_id_foreign` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksis` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
